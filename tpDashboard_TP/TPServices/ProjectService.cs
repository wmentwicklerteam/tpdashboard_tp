﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TestThinkprojectAPI.Dtos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace TestThinkprojectAPI.TPServices
{
    public class ProjectService
    {
        public async Task<List<ProjectListItem>> GetAll(string bearertoken, IOptions<ThinkProjectSettings> tpsettings)
        {
            List<ProjectListItem> projectlist = new List<ProjectListItem>();
            string applicationCode = tpsettings.Value.applicationCode;
            string apiBaseUrl = tpsettings.Value.apiBaseUrl;
            string apiProjectsUrl = tpsettings.Value.apiProjectsUrl;
            string apiurl = apiBaseUrl + apiProjectsUrl + "?page_size=500";

            var client = new RestClient(apiurl);
            var request = new RestRequest(Method.GET);
            request.AddHeader("X-TP-APPLICATION-CODE", applicationCode);
            request.AddHeader("Authorization", bearertoken);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/json");
            IRestResponse response = client.Execute(request);
            //IRestResponse<T> response = await client.ExecuteTaskAsync<T>(request);
            if (!response.IsSuccessful)
            {
                throw new Exception(response.StatusDescription);
            }
            TPProjectQueryResult queryResult = JsonConvert.DeserializeObject<TPProjectQueryResult>(response.Content);
            foreach (TPProject project in queryResult.projects)
            {
                string[] parts = project.href.Split('/');
                int len = parts.Length;
                ProjectListItem listitem = new ProjectListItem();
                if (len > 0)
                {
                    listitem.projectKey = parts[len - 1];
                }
                listitem.projectName = project.title;
                projectlist.Add(listitem);
            }

            return projectlist;
        }

    }
}
