﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TestThinkprojectAPI.Dtos;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;

namespace TestThinkprojectAPI.TPServices
{
    public class UserService 
    {
        public async Task<User> Login(string username, string password, IOptions<ThinkProjectSettings> tpsettings)
        {
            User loginuser = new User();
            string userpasswd = "username=" + username  +"&rsa=0&password=" + password;
            string applicationCode = tpsettings.Value.applicationCode;
            string apiBaseUrl = tpsettings.Value.apiBaseUrl;
            string apiAuthUrl = tpsettings.Value.apiAuthUrl;
            string apiurl = apiBaseUrl + apiAuthUrl;

            var client = new RestClient(apiurl);
            var request = new RestRequest(Method.POST);
            request.AddHeader("X-TP-APPLICATION-CODE", applicationCode);
            request.AddHeader("cache-control", "no-cache");
            request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
            request.AddParameter("undefined", userpasswd, ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            //IRestResponse<T> response = await client.ExecuteTaskAsync<T>(request);
            Token token = JsonConvert.DeserializeObject<Token>(response.Content);
            loginuser.token = token.token;
            loginuser.username = username;
            if (loginuser.token.Length > 0)
            {
                // session info
                string apiSessionUrl = tpsettings.Value.apiSessionUrl;
                apiurl = apiBaseUrl + apiSessionUrl;
                string auth = "Bearer " + loginuser.token;
                client = new RestClient(apiurl);
                request = new RestRequest(Method.GET);
                request.AddHeader("Authorization", auth);
                request.AddHeader("X-TP-APPLICATION-CODE", applicationCode);
                request.AddHeader("cache-control", "no-cache");
                request.AddHeader("Content-Type", "application/json");
                response = client.Execute(request);
                TPSessionInfo sessionInfo = JsonConvert.DeserializeObject<TPSessionInfo>(response.Content);
                string memberTitle = sessionInfo.session.authenticated_member.title;
                string[] parts = memberTitle.Split(',');
                if (parts.Length > 0)
                {
                    loginuser.lastname = parts[0];
                }
                if (parts.Length > 1)
                {
                    loginuser.firstname = parts[1];
                }
            }

            return loginuser;
        }
    }
}
