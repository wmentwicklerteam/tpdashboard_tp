﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TestThinkprojectAPI.Dtos;
using TestThinkprojectAPI.TPServices;

namespace TestThinkprojectAPI.Controllers
{
    [Route("api/projects")]
    public class ProjectListItemsController : Controller
    {
        private static ProjectService _projectService = new ProjectService();
        private readonly IOptions<ThinkProjectSettings> _tpsettings;

        public ProjectListItemsController(IOptions<ThinkProjectSettings> tpsettings)
        {
            this._tpsettings = tpsettings;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            List<ProjectListItem> list = new List<ProjectListItem>();
            string token = Request.Headers["tptoken"].ToString();
            if (token.Length == 0)
            {
                return Unauthorized();
            }
            try
            {
                list = await _projectService.GetAll(token, _tpsettings);
            }
            catch (Exception e)
            {
                return Unauthorized(e.Message);
            }
            return Ok(list);
        }
    }
}
