﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using TestThinkprojectAPI.Dtos;
using TestThinkprojectAPI.TPServices;

namespace TestThinkprojectAPI.Controllers
{
    [Route("api/[controller]")]
    public class UsersController : Controller
    {
        private static UserService _userService = new UserService();
        private readonly IOptions<ThinkProjectSettings> _tpsettings;

        public UsersController(IOptions<ThinkProjectSettings> tpsettings)
        {
            this._tpsettings = tpsettings;
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginInfo logininfo)
        {
            var user = await _userService.Login(logininfo.username, logininfo.password, _tpsettings);
            if (user.token.Length > 0)
            {
                return Ok(user);
            } else
            {
                return Unauthorized();
            }
        }
    }
}
