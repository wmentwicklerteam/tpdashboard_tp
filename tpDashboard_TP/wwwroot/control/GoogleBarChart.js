sap.ui.define([
  "sap/ui/thirdparty/jquery",
  "sap/ui/core/HTML",
  "./BaseChart",
  '../control/googlechart/googlechart',
  "sap/ui/core/theming/Parameters"
], function ($, HTML, BaseChart, Parameters) {
  "use strict";

  return BaseChart.extend("wum.demo.testtpapi.control.GoogleBarChart", {


    init: function () {
      BaseChart.prototype.init.call(this);
      this.setType("Bar");
    },
    //http://bl.ocks.org/d3noob/b3ff6ae1c120eea654b5
    //https://stackoverflow.com/questions/43226648/how-to-convert-json-to-google-visualization-datatable

    _updateSVG: function (iWidth) {
      var jsonData = this.getBinding("data").getCurrentContexts().map(function (oContext) {
        return oContext.getObject();
      });

      var arrData = [];
      if (jsonData[1].length > 0) {
        // load column headings
        var colHead = [];
        Object.keys(jsonData[1][0]).forEach(function (key) {
          colHead.push(key);
        });

        colHead = colHead.map(function (item) {
          return item == colHead[2] ? {
            role: "style"
          } : item;
        });
        arrData.push(colHead);

        // load data rows
        jsonData[1].forEach(function (row) {
          var arrRow = [];
          Object.keys(row).forEach(function (key) {
            arrRow.push(row[key]);
          });
          arrData.push(arrRow);
        });
      };
      // console.log(arrData);
      var that = this;
      google.charts.load('current', {
        'packages': ['corechart']
      });
      google.charts.setOnLoadCallback(function () {
        var data = google.visualization.arrayToDataTable(arrData);

        var view = new google.visualization.DataView(data);
        view.setColumns([0, 1,
          {
            calc: "stringify",
            sourceColumn: 1,
            type: "string",
            role: "annotation"
          },
          2
        ]);

        var options = {
          title: jsonData[0],
          width: iWidth,
          fontSize: 11,
          bold: true,
          italic: true ,
          bar: {
            groupWidth: "95%"
          },
          legend: {
            position: "none"
          },
          chartArea: {
            left: 160,
            right: 20,
            top: 40,
            bottom: 20,
            width: '100%',
            height: '100%'
          },
        
        };

        var chart = new google.visualization.BarChart(that.$()[0]);
        chart.draw(view, options);

      }, 1000);


    },

    renderer: function () {
      BaseChart.prototype.getRenderer().render.apply(this, arguments);
    }
  });
});