sap.ui.define([
    "sap/ui/thirdparty/jquery",
    "sap/ui/core/HTML",
    "./BaseChart",
    '../control/googlechart/googlechart',
    "sap/ui/core/theming/Parameters"
], function ($, HTML, BaseChart, Parameters) {
    "use strict";

    return BaseChart.extend("wum.demo.testtpapi.control.GoogleLineChart", {


        init: function () {
            BaseChart.prototype.init.call(this);
            this.setType("Line");
        },
        //http://bl.ocks.org/d3noob/b3ff6ae1c120eea654b5

        _updateSVG: function (iWidth) {
            var jsonData = this.getBinding("data").getCurrentContexts().map(function (oContext) {
                return oContext.getObject();
            });
          

           // var coltitle = this.getModel("line").oData.title;

            var arrData = [];
            if (jsonData[1].length > 0) {
                // load line headings
                var colHead = [];
                Object.keys(jsonData[1][0]).forEach(function (key) {
                    colHead.push(key);
                });

                arrData.push(colHead);

                // load data rows
                jsonData[1].forEach(function (row) {
                    var arrRow = [];
                    Object.keys(row).forEach(function (key) {
                        arrRow.push(row[key]);
                    });
                    arrData.push(arrRow);
                });
            };

            var that = this;

            google.charts.load('current', {
                'packages': ['line']
            });
            google.charts.setOnLoadCallback(function () {
                var data = google.visualization.arrayToDataTable(arrData);

                var options = {
                   title: jsonData[0],
                    curveType: 'function',
                    legend: {
                        position: 'bottom'
                    },
                    width: iWidth,
                    series: {
                        0: { color: '#234E85' },
                        1: { color: '#6E7C74' },
                        2: { color: '#C7B763' },
                        3: { color: '#D09D48' },
                        4: { color: '#CA6220' },
                        5: { color: '#C63501' },
                      },
                    chartArea:{left:35,right:20,top:40,bottom:30,width:'100%',height:'150%'}
                };

                var chart = new google.visualization.LineChart(that.$()[0]);
                chart.draw(data, options);

            }, 1000);

        },

        renderer: function () {
            BaseChart.prototype.getRenderer().render.apply(this, arguments);
        }
    });
});