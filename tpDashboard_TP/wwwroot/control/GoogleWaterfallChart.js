sap.ui.define([
    "sap/ui/thirdparty/jquery",
    "sap/ui/core/HTML",
    "./BaseChart",
    '../control/googlechart/googlechart',
    "sap/ui/core/theming/Parameters"
], function ($, HTML, BaseChart, Parameters) {
    "use strict";

    return BaseChart.extend("wum.demo.testtpapi.control.GoogleWaterfallChart", {


        init: function () {
            BaseChart.prototype.init.call(this);
            this.setType("Waterfall");
        },
        //http://bl.ocks.org/d3noob/b3ff6ae1c120eea654b5

        _updateSVG: function (iWidth) {
            var jsonData = this.getBinding("data").getCurrentContexts().map(function (oContext) {
                return oContext.getObject();
            });
            var arrData = [];
            var arr = [];
            if (jsonData[1].length > 0) {

                // load data rows
                jsonData[1].forEach(function (row) {
                    var arrRow = [];
                    Object.keys(row).forEach(function (key) {
                        arrRow.push(row[key]);
                    });
                    arrData.push(arrRow);
                });

                for (var i = 0; i < arrData.length - 1; i++) {
                    arr.push([]);
                    arr[i].push(arrData[i][0], arrData[i][1], arrData[i][1], arrData[i + 1][1], arrData[i + 1][1]);

                }
                // console.log(arr);

            };
            var that = this;
            google.charts.load('current', {
                'packages': ['corechart']
            });
            google.charts.setOnLoadCallback(function () {
                var data = google.visualization.arrayToDataTable(arr, true);

                var options = {
                    title: jsonData[0],
                    legend: 'none',
                    bar: {
                        groupWidth: '100%'
                    }, // Remove space between bars.
                    width: iWidth,
                    chartArea: {
                        left: 45,
                        right: 20,
                        top: 40,
                        bottom: 30,
                        width: '100%',
                        height: '150%'
                    },
                    candlestick: {
                        fallingColor: {
                            strokeWidth: 0,
                            fill: '#7C7C7C'
                        }, // red
                        risingColor: {
                            strokeWidth: 0,
                            fill: '#5A7D9F'
                        } // green
                    }
                };

                var chart = new google.visualization.CandlestickChart(that.$()[0]);

                chart.draw(data, options);
            }, 1000);

        },

        renderer: function () {
            BaseChart.prototype.getRenderer().render.apply(this, arguments);
        }
    });
});