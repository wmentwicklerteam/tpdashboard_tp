﻿sap.ui.define([
    "sap/ui/model/json/JSONModel",
    "sap/ui/Device"
], function (JSONModel, Device) {
    "use strict";

    return {
        createDeviceModel: function () {
            var oModel = new JSONModel(Device);
            oModel.setDefaultBindingMode("OneWay");
            return oModel;
        },

        createMessageModel: function () {
            var oMessageData = {
                msgText: "",
                msgVisible: false,
                msgType: sap.ui.core.MessageType.Information
            };
            var oModel = new JSONModel(oMessageData);
            return oModel;
        },

        createUserInfoModel: function () {
            var oUserInfoData = {
                username: "",
                displayName: "",
                email: "",
                lastname: "",
                firstname: "",
                isAdmin: false,
                password: "",
                token: ""
            };
            var oModel = new JSONModel(oUserInfoData);
            return oModel;
        }
    };

});