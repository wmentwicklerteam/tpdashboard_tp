﻿sap.ui.define([
	'./BaseController',
	'sap/ui/model/json/JSONModel',
	"sap/ui/VersionInfo",
	"sap/ui/core/mvc/XMLView",
	"sap/ui/core/UIComponent",
], function (BaseController, JSONModel, VersionInfo, XMLView,UIComponent) {
	"use strict";
	
	return BaseController.extend("wum.demo.testtpapi.controller.Home", {

		onInit: function () {		
			
			
			//Create the model and load data from the local JSON file
			var oModel = new JSONModel("./model/projectlist.json");
			oModel.setDefaultBindingMode("OneWay");
			// set the device model
			this.setModel(oModel, "projectlist");
			
			var oBarModel = new JSONModel("./model/bardata.json");
			oBarModel.setProperty("/title", "Anzahl der Dokumente je Projekt");
			this.getView().setModel(oBarModel, 'bar');


			var oDonutModel = new JSONModel("./model/donutdata.json");
			oDonutModel.setProperty("/title", "Anzahl der Dokumente je Projekt");
			this.getView().setModel(oDonutModel, 'donut');

			var oDonut2Model = new JSONModel("./model/donut2data.json");
			oDonut2Model.setProperty("/title", "Anzahl der Dokumente je Projekt");
			this.getView().setModel(oDonut2Model, 'donut2');

			var oDonut3Model = new JSONModel("./model/donut3data.json");
			oDonut3Model.setProperty("/title", "Anzahl der Dokumente je Projekt");
			this.getView().setModel(oDonut3Model, 'donut3');
		


			
			


		},

		

		onRefresh: function () {
			this.byId("charts").byId("statisticsBlockLayout").invalidate();
			this.byId("charts").byId("statisticsBlockLayout").setBusy(true);
			setTimeout(function () {
				this.byId("charts").byId("statisticsBlockLayout").setBusy(false);
			}.bind(this), 2000);
		}

	});
});