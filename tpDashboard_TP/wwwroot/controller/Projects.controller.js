﻿sap.ui.define([
    'wum/demo/testtpapi/controller/BaseController',
    'sap/ui/model/json/JSONModel',
    'sap/ui/Device'
], function (BaseController, JSONModel, Device) {
    "use strict";
        return BaseController.extend("wum.demo.testtpapi.controller.Projects", {
            onInit: function () {
                this.dataReload = true;

                var oViewModel;

                // Model used to manipulate control states
                oViewModel = new JSONModel({
                    worklistTableTitle: this.getResourceBundle().getText("titleProjects"),
                    tableNoDataText: this.getResourceBundle().getText("txtTableNoData"),
                    tableBusyDelay: 0,
                    tableFilter: ""
                });
                this.setModel(oViewModel, "worklistView");

                var eventBus = sap.ui.getCore().getEventBus();
                eventBus.subscribe("app", "loggedout", this.onLoggedOut, this);

                var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
                oRouter.getRoute("projects").attachMatched(this.onRouteMatched, this);
            },

            onRouteMatched: function (oEvent) {
                if (this.dataReload !== false) {
                    this._loadData();
                }
            },

            _loadData: function () {
                var _this = this;
                var oView = this.getView();
                var oApp = this.getOwnerComponent().byId("app");
                var oTable = this.byId("tblProjects");

                // set the domain model
                var oModel = new JSONModel();
                oModel.attachRequestSent(function () {
                    oApp.setBusy(true);
                });
                oModel.attachRequestCompleted(function () {
                    oApp.setBusy(false);
                    oView.setModel(oModel);
                    _this.dataReload = false;
                });
                // get service URL
                var sURL = this.getUrlApi("projects");
                oModel.loadData(sURL, "", true, "GET", false, false, this.authHeaders());
            },

            onProjectDetailViewPress: function (oEvent) {
            },

            onLoggedOut: function () {
                this.dataReload = true;
            },

            clearAllFilters: function (oEvent) {
            }
    });
});
