﻿sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"./model/models"
], function(UIComponent, Device, models) {
	"use strict";

	return UIComponent.extend("wum.demo.testtpapi.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function() {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

			// create the views based on the url/hash
			this.getRouter().initialize();
			jQuery.sap.registerModulePath("i2d.pp.mrpcockpit.reuse", "/sap/bc/ui5_ui5/sap/pp_mrp_reuse/i2d/pp/mrpcockpit/reuse/"); 
		}
	});
});