﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestThinkprojectAPI.Dtos
{
    public class TPObject
    {
        public string href { get; set; }
        public string rel { get; set; }
        public string title { get; set; }

        public TPObject()
        {
            href = string.Empty;
            rel = string.Empty;
            title = string.Empty;
        }
    }

    public class TPLink : TPObject
    {
        public string about { get; set; }
        public string method { get; set; }
        public string type { get; set; }
        public string required { get; set; }

        public TPLink() : base()
        {
            about = string.Empty;
            method = string.Empty;
            type = string.Empty;
            required = string.Empty;
        }
    }

    public class TPApplication : TPObject
    {
        public TPApplication() : base()
        {
        }
    }

    public class TPAuthenticatedMember : TPObject
    {
        public TPAuthenticatedMember() : base()
        {
        }
    }

    public class TPMember : TPObject
    {
        public TPMember() : base()
        {
        }
    }

    public class TPProject : TPObject
    {
        public TPProject() : base()
        {
        }
    }

    public class TPEnvironment
    {
        public string rest { get; set; }
        public string scripting { get; set; }
        public string standard { get; set; }

        public TPEnvironment()
        {
            rest = string.Empty;
            scripting = string.Empty;
            standard = string.Empty;
        }
    }

    public class TPHttpRequest
    {
        public string body { get; set; }
        public string method { get; set; }
        public string url { get; set; }

        public TPHttpRequest()
        {
            body = string.Empty;
            method = string.Empty;
            url = string.Empty;
        }
    }

    public class TPMetaData
    {
        public string description { set; get; }
        public int last_page { set; get; }
        public int page_offset { set; get; }
        public int page_size { set; get; }
        public string query_template { set; get; }
        public int total { set; get; }
        public string urn { set; get; }

        public TPMetaData()
        {
            description = string.Empty;
            last_page = 0;
            page_offset = 0;
            page_size = 0;
            query_template = string.Empty;
            total = 0;
            urn = string.Empty;
        }
    }

    public class TPSession
    {
        public TPApplication application { get; set; }
        public bool authenticated { get; set; }
        public TPAuthenticatedMember authenticated_member { get; set; }
        public string cookies { get; set; }
        public TPEnvironment environment { get; set; }
        public TPHttpRequest http_request { get; set; }
        public string language { get; set; }
        public string last_diag { get; set; }
        public TPMember member { get; set; }
        public bool member_modifiable { get; set; }
        public TPProject project { get; set; }
        public string token { get; set; }

        public TPSession()
        {
            application = new TPApplication();
            authenticated = false;
            authenticated_member = new TPAuthenticatedMember();
            cookies = string.Empty;
            environment = new TPEnvironment();
            http_request = new TPHttpRequest();
            language = string.Empty;
            last_diag = string.Empty;
            member = new TPMember();
            member_modifiable = false;
            project = new TPProject();
            token = string.Empty;
        }
    }

    public class TPSessionInfo
    {
        public IList<TPLink> links { get; set; }
        public TPSession session { get; set; }

        public TPSessionInfo()
        {
            links = new List<TPLink>();
            session = new TPSession();
        }
    }

    public class TPProjectQueryResult
    {
        public IList<TPLink> links { get; set; }
        public TPMetaData meta { get; set; }
        public IList<TPProject> projects { get; set; }

        public TPProjectQueryResult()
        {
            links = new List<TPLink>();
            meta = new TPMetaData();
            projects = new List<TPProject>();
        }
    }

}
