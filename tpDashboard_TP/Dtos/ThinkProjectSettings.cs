﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestThinkprojectAPI.Dtos
{
    public class ThinkProjectSettings
    {
        public string applicationCode { get; set; }
        public string apiBaseUrl { get; set; }
        public string apiAuthUrl { get; set; }
        public string apiSessionUrl { get; set; }
        public string apiProjectsUrl { get; set; }

        public ThinkProjectSettings()
        {
            applicationCode = string.Empty;
            apiBaseUrl = string.Empty;
            apiAuthUrl = string.Empty;
            apiSessionUrl = string.Empty;
            apiProjectsUrl = string.Empty;
        }
    }
}
