﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestThinkprojectAPI.Dtos
{
    public class User
    {
        public string username { set; get; }
        public string lastname { set; get; }
        public string firstname { set; get; }
        public string token { set; get; }

        public User()
        {
            username = string.Empty;
            lastname = string.Empty;
            firstname = string.Empty;
            token = string.Empty;
        }
    }
}
