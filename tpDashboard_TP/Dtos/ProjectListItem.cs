﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestThinkprojectAPI.Dtos
{
    public class ProjectListItem
    {
        public string projectKey { set; get; }
        public string projectName { set; get; }

        public ProjectListItem()
        {
            projectKey = string.Empty;
            projectName = string.Empty;
        }
    }
}
