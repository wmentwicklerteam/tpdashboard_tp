﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestThinkprojectAPI.Dtos
{
    public class LoginInfo
    {
        public string username { set; get; }
        public string password { set; get; }

        public LoginInfo()
        {
            username = string.Empty;
            password = string.Empty;
        }
    }


    public class Token
    {
        public string token { set; get; }

        public Token()
        {
            token = string.Empty;
        }
    }
}
